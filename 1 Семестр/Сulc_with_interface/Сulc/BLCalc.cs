﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Сulc
{
    class BLCulc:ITry
    {        
        private double _rez;

            public double Add(double _firstNumber,double _secondNumber)
            {
                _rez = _firstNumber + _secondNumber;
                //Console.WriteLine("{0}+{1}={2} ", _firstNumber, _secondNumber, _rez);
                return _rez;
            }

            public double Sub(double _firstNumber, double _secondNumber)
            {
                _rez = _firstNumber - _secondNumber;
//Console.WriteLine("{0}-{1}={2} ",_firstNumber,_secondNumber, _rez);
                return _rez;
            }

            public double Mult(double _firstNumber, double _secondNumber)
            {
                _rez = _firstNumber * _secondNumber;
               // Console.WriteLine("{0}*{1}={2} ", _firstNumber, _secondNumber, _rez);
                return _rez;
            }

            public double Division(double _firstNumber, double _secondNumber)
            {
                if (_secondNumber == 0)
                    Console.WriteLine("Error");
                else
                {
                    _rez = _firstNumber / _secondNumber;
                    //Console.WriteLine("{0}/{1}={2} ", _firstNumber, _secondNumber, _rez);
                }

                return _rez;
            }

            public double SinA(double A)
            {
            return Math.Sin(A);
            }
            
            public double CosA(double A)
            {
            return Math.Cos(A);
            }
            
            public double TanA(double A)
             {
            return Math.Tan(A);
             }
            
            public double CtgA(double A)
             {
            if (Math.Sin(A) == 0)
                Console.WriteLine("Error! Sin A = 0");
            
            return Math.Cos(A) / Math.Sin(A);
             }
        }
    }
