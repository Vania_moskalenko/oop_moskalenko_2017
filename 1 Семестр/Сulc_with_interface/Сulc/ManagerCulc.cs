﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Сulc
{
    class ManCulc
    {
        private double _firstNumber { get; set; }
        private double _secondNumber { get; set; }
        private double A { get; set; }


        private BLCulc Ex = new BLCulc();
        public double InputFirstNum()
        {
            Console.WriteLine("Input first number");
            try
            {
                _firstNumber = Convert.ToDouble(Console.ReadLine());
            }

            catch (FormatException)
            {
                Console.WriteLine("Error");
                Console.WriteLine("Input first number");
                _firstNumber = Convert.ToDouble(Console.ReadLine());
            }
            return _firstNumber;
        }

        public double InputSecondNum()
        {
            Console.WriteLine("Input second number");
            try
            {
                _secondNumber = Convert.ToDouble(Console.ReadLine());
            }

            catch (FormatException)
            {
                Console.WriteLine("Error");
                Console.WriteLine("Input second number");
                _secondNumber = Convert.ToDouble(Console.ReadLine());

            }

            return _secondNumber;
        }

        public double InputA()
        {
            try
            {
                Console.WriteLine("Input Alfa");
                A = Convert.ToDouble(Console.ReadLine());
            }
            catch(FormatException)
            {
                Console.WriteLine("Error");
                Console.WriteLine("Input Alfa");
                A = Convert.ToDouble(Console.ReadLine());
            }
            return A;
        }



        public double Switch(double _firstNumber, double _secondNumber)
        {
            double rez = 0;
            Console.WriteLine("Switch mathematical action\n 1.Add\n 2.Subtraction\n 3.Mult\n 4.Division\n ");
            int Action = Convert.ToInt32(Console.ReadLine());


            switch (Action)
            {
                case 1:
                    {
                        rez = Ex.Add(_firstNumber, _secondNumber);
                        // return rez;
                        break;
                    }
                case 2:
                    {
                        rez = Ex.Sub(_firstNumber, _secondNumber);
                        // return rez;
                        break;
                    }
                case 3:
                    {
                        rez = Ex.Mult(_firstNumber, _secondNumber);
                        // return rez;
                        break;
                    }
                case 4:
                    {
                        rez = Ex.Division(_firstNumber, _secondNumber);
                        //return rez;
                        break;
                    }


            }
            return rez;
        }

        public double ChooseTrigAction(double A)
        {
            double rez = 0;
            Console.WriteLine("Switch mathematical action\n  1.SinA\n 2.CosA\n 3.TgA\n 4.CtgA\n");
            int Action = Convert.ToInt32(Console.ReadLine());

            switch (Action)
            {
                case 1:
                    {
                        rez = Ex.SinA(A);
                        // return rez;
                        break;
                    }
                case 2:
                    {
                        rez = Ex.CosA(A);
                        // return rez;
                        break;
                    }
                case 3:
                    {
                        rez = Ex.TanA(A);
                        //return rez;
                        break;
                    }

                case 4:
                    {
                        rez = Ex.CtgA(A);
                        //return rez;
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Error!");
                        break;
                    }
                    
            }
            return rez;
        }
    }
}
