﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Сulc
{
    class ManCulc
    {
        private double _firstNumber { get; set; }
        private double _secondNumber { get; set; }


        private BLCulc Ex = new BLCulc();
        public double InputFirstNum()
        {
            Console.WriteLine("Input first number");
            try
            {
                _firstNumber = Convert.ToDouble(Console.ReadLine());
            }

            catch (FormatException)
            {
                Console.WriteLine("Error");
                Console.WriteLine("Input first number");
            } 
           return _firstNumber;
        }

        public double InputSecondNum()
        {
            Console.WriteLine("Input second number");
            try
            {
                _secondNumber = Convert.ToDouble(Console.ReadLine());
            }

            catch (FormatException)
            {
                Console.WriteLine("Error");
                Console.WriteLine("Input second number");
                _secondNumber = Convert.ToDouble(Console.ReadLine());

            }

            return _secondNumber;
        }
            

                   
        public void Switch(double _firstNumber, double _secondNumber)
        {
            
            Console.WriteLine("Switch mathematical action\n 1.Add\n 2.Subtraction\n 3.Mult\n 4.Division\n");
            int Action = Convert.ToInt32(Console.ReadLine());


            switch (Action)
            {
                case 1:
                    {
                        Ex.Add( _firstNumber,  _secondNumber);
                        break;
                    }
                case 2:
                    {
                        Ex.Sub(_firstNumber, _secondNumber);
                        break;
                    }
                case 3:
                    {
                       Ex.Mult(_firstNumber, _secondNumber);
                        break;
                    }
                case 4:
                    {
                       Ex.Division(_firstNumber, _secondNumber);
                       
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Error!");
                        break;
                    }
                    
            }
        }
        
    }
}
