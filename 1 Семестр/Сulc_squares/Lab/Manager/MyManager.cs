﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab
{
    class MyManager
    {
        private double _side1;
        private double _Alfa;
        private double _square;
        private LBClass Helper = new LBClass();

        public double Check()
        {
            double size1 = 0;
            try
            {
                 size1 = Convert.ToInt32(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("Ошибка!");
                 size1 = Convert.ToInt32(Console.ReadLine());
            }
            return size1;
        }
        public void Triangle(double _side1)

        {
            double Per = _side1*3;
            Console.WriteLine("Периметр = {0}", Per);
            _square = Helper.MethodOfGeron(_side1);
            Console.WriteLine("Площадь = {0}", _square);
        }

        public void Square(double _side1)

        {
            double Per = _side1*4;
            Console.WriteLine("Периметр = {0}", Per);
            _square = Helper.Quad(_side1);
            Console.WriteLine("Площадь = {0}", _square);
        }

        public void Multyculus(double _side1)
        {
            Console.WriteLine("Введите количество сторон");
            int num = Convert.ToInt32(Console.ReadLine());
            double Per = _side1 * num;
            Console.WriteLine("Периметр = {0}", Per);
            _square = Helper.Multiculus(_side1, num);
            Console.WriteLine("Площадь = {0}", _square);

        }
    }
}
