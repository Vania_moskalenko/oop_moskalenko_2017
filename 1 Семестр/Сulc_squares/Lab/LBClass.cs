﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab
{
    class LBClass
    {
        // Dlya nahojdeniya treygolnika
        public double MethodOfGeron(double sideA)
        {
            //formula Gerona
            double sidePP = (sideA + sideA + sideA) / 2;
            return Math.Sqrt(sidePP * (sidePP - sideA) * (sidePP - sideA) * (sidePP - sideA));
        }
        

        public double Quad(double A)
        {
            return Math.Pow(A,2);
        }
        public double Multiculus(double A, int Number)
        {
            return (Number * Math.Pow(A, 2)) * (4 * Math.Tan(360 /(2* Number)));
        }
    }
}
