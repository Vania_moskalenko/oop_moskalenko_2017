﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_1
{
    public class Student
    {
        public string _name;
        public int _age;
        public string _group;
        public int _goldMedalCheck;
        public string _GoldMedal;

        public string Name { get { return _name; } set { _name = value; } }
        public int Age { get { return _age; } set { _age = value; } }
        public string Group { get { return _group; } set { _group = value; } }
        public string Gold_Medal { get { return _GoldMedal; } set { _GoldMedal = value; } }
    
        public void Input()
        {
            Console.Write("Enter the student's name (only letters): ");
            _name = Convert.ToString(Console.ReadLine());
            try
            {
                Console.Write("Enter the student's age (only figures bigger than 0): ");
                _age = Convert.ToInt32(Console.ReadLine());
                if (_age <= 0)
                {
                    throw new ArgumentOutOfRangeException();
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                Console.WriteLine("ERROR!\nSorry, but you should enter numbers greater 0");
                Console.Write("Enter the student's age (only figures bigger than 0): ");
                _age = Convert.ToInt32(Console.ReadLine());
            }
            Console.Write("Enter the student's group (use letters and symbols): ");
            _group = Convert.ToString(Console.ReadLine());
            Console.Write("Does a student have a gold medal?\n 1- Student has gold medal\n 0- Student hasn`t gold medal\n: ");
            _goldMedalCheck = Convert.ToInt32(Console.ReadLine());
            try
            {
                if (_goldMedalCheck == 1)
                    _GoldMedal = "Have gold medal";
                else 
                    _GoldMedal = "Haven`t gold medal";

                if (_goldMedalCheck > 1 || _goldMedalCheck < 0)

                    throw new ArgumentOutOfRangeException();

            }
            catch
            {
                Console.WriteLine("ERROR!\nSorry, but you should enter 0 or 1");
                Console.Write("Does a student have a gold medal? \n 1- Student has gold medal; \n0- Student hasn`t gold medal: ");
                _goldMedalCheck = Convert.ToInt32(Console.ReadLine());
                if (_goldMedalCheck == 1)
                    _GoldMedal = "Have gold medal";
                else 
                    _GoldMedal = "Haven`t gold medal";
            }
        }

        public void Output()
        {
            string output = string.Format("Studen`s name: {0} \n Studen`s age: {1} \n Student`s group: {2} \n Student`s gold medal: {3}\n", _name, _age, _group, _GoldMedal);
            Console.WriteLine(output);
            Console.Read();
        }
    }
}

