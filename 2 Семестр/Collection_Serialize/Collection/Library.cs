﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Collection
{
    [Serializable]
    class Lybrary
    { }
    
    [Serializable]
    class Library
    {
        Book[] books;

        public Library()
        {
            books = new Book[] { new Book("Винетту на Диком Западе"), new Book("Война и мир"),
                new Book("Евгений Онегин") };
        }

        public int Length
        {
            get { return books.Length; }
        }

        public Book this[int index]
        {
            get
            {
                return books[index];
            }
            set
            {
                books[index] = value;
            }
        }

    }
    [Serializable]
    class Book
    {
        public Book(string name)
        {
            this.Name = name;
        }
        public string Name { get; set; }
    }



}
