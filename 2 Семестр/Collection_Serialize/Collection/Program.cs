﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Xml.Serialization;

namespace Collection
{
    class Program
    {
        static void Main(string[] args)
        {
            Library library = new Library();

            Console.WriteLine(library[0].Name);

            library[0] = new Book("Мастер и Маргарита");
            Console.WriteLine(library[0].Name);

            BinaryFormatter bfm = new BinaryFormatter();
            using (FileStream fs = new FileStream("Library.dat", FileMode.OpenOrCreate))
            {
                bfm.Serialize(fs, library);

                Console.WriteLine("Cериализация завершена");
            }

            XmlSerializer xfm = new XmlSerializer(typeof(Library));

            using (FileStream fs = new FileStream("Library.xml", FileMode.OpenOrCreate))
            {
                xfm.Serialize(fs, library);

                Console.WriteLine(" Cериализация завершена");
            }

            using (FileStream fs = new FileStream("Library.dat", FileMode.OpenOrCreate))
            {
                Library newLibrary = (Library)bfm.Deserialize(fs);

                Console.WriteLine("Объект десериализован");
                Console.WriteLine("Название Книги: ", library[0].Name);
            }


                using (FileStream fs = new FileStream("Library.xml", FileMode.OpenOrCreate))
            {
                Library newLibrary = (Library)xfm.Deserialize(fs);

                Console.WriteLine("Объект десериализован");
                Console.WriteLine("Название Книги: ", library[0].Name);
            }


            Console.ReadLine();
        }
    }
}

