﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication6
{
    public class MyLIst
    {
        private List<int> num = new List<int>();
        private int x;
        private int index;


        public void addToList(int x)
        {
            num.Add(x);
        }

        public void removeFromList(int x)
        {
            num.Remove(x);
        }

        public void insertToList(int index, int x)
        {
            num.Insert(index, x);
        }

        public void lookList()
        {
            for(int i=0; i<num.Count;i++)
            {
                Console.WriteLine(num[i]);
            }
        }

    }
}
