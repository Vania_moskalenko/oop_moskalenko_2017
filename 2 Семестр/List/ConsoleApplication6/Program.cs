﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication6
{
    class Program
    {
        static void Main(string[] args)
        {
            EventsClass ec = new EventsClass();
            ListManager lm = new ListManager();
            lm.Start += lm.ChooseAction;
            lm.ChooseAction();

            Console.ReadKey();

        }
    }
}
